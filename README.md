Năm 2007, Lê Viết Lam quyết định trở về đầu tư tại Việt Nam. Công trình lớn đầu tiên ở trong nước của Sun Group chính là Khu du lịch Bà Nà Hills tại Đà Nẵng. Đây là một tổ hợp dự án với hệ thống cáp treo dài hơn 5.801m với tổng kinh phí đầu tư hơn 30 triệu euro (hơn 34 triệu USD), Làng Pháp với tổng vốn đầu tư 70 triệu USD cùng Công viên Fantasy (Fantasy Park).

Cũng tại Đà Nẵng trên đường 2/9 dọc theo bờ Tây sông Hàn, ngay chân cầu Tuyên Sơn, Sun Group đầu tư Công viên Châu Á (Asia Park), khu công viên vui chơi giải trí, với tổng vốn đầu tư lên đến 10.000 tỷ đồng. Công viên nằm trên diện tích đất khoảng 89 ha.
Tuyến cáp treo Fansipan Sapa được khởi công vào tháng 11/2013 và khai mạc ngày 2.2.2016 
Tại Quảng Ninh, Sun Group 2017 đang triển khai tổ hợp dự án Công viên Đại Dương tại thành phố Hạ Long. Được thiết kế theo mô hình công viên Disneyland, dự án này có vốn đầu tư lên tới 6.000 tỷ đồng.

Website: https://duan-sungroup.com/
Địa Chỉ: Tầng 19, Tòa nhà Vincom Đồng Khởi, Quận 1, TP HCM..
Điện Thoại: 0933 53 77 53 
Email: sungroup.sale@gmail.com